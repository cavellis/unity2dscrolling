﻿using UnityEngine;
using System.Collections;

public class ScrollingBackgroundManager : MonoBehaviour 
{
	public GameObject[] backgrounds;
	public int scrollSpeed = 2;

	private Vector3 scrollAmount;
	private float worldWidth;
	private float backgroundWidth;

	void Start () 
	{
		scrollAmount = new Vector3 (-scrollSpeed, 0, 0);
		worldWidth = GetWorldWidth ();

		GameObject obj = backgrounds [0];
		SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
		backgroundWidth = renderer.bounds.size.x;
		Debug.Log ("background width: " + backgroundWidth);

		Debug.Log ("pixelWidth? " + Camera.main.pixelWidth);
		Debug.Log ("screen w? " + Screen.width);
	}
	
	void Update () 
	{
		foreach (GameObject obj in backgrounds) 
		{
			obj.transform.Translate(scrollAmount);
			Vector3 objPos = obj.transform.position;
			float objRight = objPos.x + backgroundWidth/2;
			if((objRight) <= -GameConstants.SCREEN_WIDTH/2)
			{
				float newX = objPos.x + worldWidth;
				Vector3 pos = new Vector3(newX, 0, 0);
				obj.transform.position = pos;
			}
		}
	}

	public float GetWorldWidth()
	{
		//only works if all backgrounds are the same size
		GameObject obj = backgrounds [0];
		SpriteRenderer renderer = obj.GetComponent<SpriteRenderer>();
		float backgroundWidth = renderer.bounds.size.x;
		float w = backgroundWidth * backgrounds.Length;
		return w;
	}
}
