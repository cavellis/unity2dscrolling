﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int speed = 32;

	private float spriteWidth;
	private float worldWidth;

	void Start () 
	{
		SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
		spriteWidth = renderer.bounds.size.x;

		ScrollingBackgroundManager mgr = GameObject.Find ("ScrollingBackgroundManager").GetComponent<ScrollingBackgroundManager> ();
		worldWidth = mgr.GetWorldWidth ();
		Debug.Log ("world width: " + worldWidth);

	}
	
	void Update () 
	{
		transform.Translate (new Vector3 (-speed, 0, 0));

		Vector3 pos = transform.position;
		if ((pos.x + spriteWidth/2) < -GameConstants.SCREEN_WIDTH/2) 
		{
			Vector3 newPos = new Vector3(pos.x + worldWidth, pos.y, pos.z);
			transform.position = newPos;
		}


	}
}
